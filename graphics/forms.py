#!/usr/bin/python
# -*- coding: utf-8 -*-

from django import forms
from django.core.exceptions import ValidationError
from .utils import parse_excel_file
from .models import Graphic
import mimetypes
def validate_file_extension(value):
    mime = mimetypes.guess_type(value.name)[0]
    if mime not in ['application/vnd.openxmlformats-officedocument.spreadsheetml.sheet','application/vnd.ms-excel']:
        raise ValidationError('Ошибка валадаций типа файла')


class GraphicForm(forms.Form):
    file = forms.FileField(validators=[validate_file_extension])
    def clean(self):
        super(forms.Form,self).clean()
        data = self.cleaned_data
        coords = parse_excel_file(self.cleaned_data['file'])
        if len(coords) == 0:
             raise ValidationError('Фаил пустой')
        data['coords'] = coords
        return data
