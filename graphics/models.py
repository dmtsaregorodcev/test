#!/usr/bin/python
# -*- coding: utf-8 -*-

from django.db import models

# Create your models here.
class Graphic(models.Model):
    x = models.FloatField()
    y = models.FloatField()