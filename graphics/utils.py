import xlrd

def parse_excel_file(file):
    coords = []
    book = xlrd.open_workbook(file_contents = file.read())
    wb_sheet = book.sheet_by_index(0)
    for rownum in range(0, wb_sheet.nrows):
        x,y = wb_sheet.row_values(rownum)
        coords.append({'x':x,'y':y})
    return coords