import json
from django.shortcuts import render
from django.http import HttpResponse
from .forms import GraphicForm
from .models import Graphic
from django.core import serializers


def main(request):
    return render(request, 'graphics/main.html')


def upload(request):
    form = GraphicForm(data=request.POST, files=request.FILES)
    result = {}
    if form.is_valid():
        Graphic.objects.all().delete()
        data = form.cleaned_data
        bulk_coords = []
        for value in data['coords']:
            graphic = Graphic(x=value['x'], y=value['y'])
            bulk_coords.append(graphic)
        Graphic.objects.bulk_create(bulk_coords)
        result['success'] = True
        result['message'] = 'Данные успешно добавленны'
    else:
        result['success'] = False
        result['message'] = 'Фаил пустой или неверное расширение'  # form.errors
    return HttpResponse(json.dumps(result), content_type='application/json; charset=UTF-8')


def coords(request):
    data = serializers.serialize('json', Graphic.objects.all())
    return HttpResponse(data, content_type='application/json; charset=UTF-8')
