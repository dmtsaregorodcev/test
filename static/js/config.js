app.config(['$httpProvider', '$interpolateProvider',
    function ($httpProvider, $interpolateProvider) {
        /* for compatibility with django teplate engine */
        $interpolateProvider.startSymbol('{$');
        $interpolateProvider.endSymbol('$}');
        /* csrf */
       $httpProvider.defaults.headers.common["X-CSRFToken"] = window.csrf_token;
    }
]);