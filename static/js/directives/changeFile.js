app.directive('changeFile', function (fileService) {
    var validExt = ['xlsx', 'xls'];
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            element.bind('change', function () {
                var file = element[0].files[0];
                if (validate(file)) {
                    fileService.setFile(file);
                } else {
                    alert('Допустимый формат ' + validExt.join(','));
                    element.val(null);
                }
            });
            function validate(file) {
                var file_extension = file.name.split('.').pop();
                for (var i = 0; i <= validExt.length; i++) {
                    if (file_extension == validExt[i]) {
                        return true;
                    }
                }
                return false;
            }
        },
    }
});