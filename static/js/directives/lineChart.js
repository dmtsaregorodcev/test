app.directive('lineChart', function (coordsService) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            scope.options = {
                width: 600,
                height: 300,
                legend: {
                    position: 'none'
                },
                title: "График"
            };
            scope.init = false;
            scope.renderChart = function () {
                if (!scope.init) {
                    scope.init = true;
                    scope.chart = new google.visualization.LineChart(document.getElementById('gChart'));
                    scope.data = new google.visualization.DataTable();
                    scope.data.addColumn('number', 'X');
                    scope.data.addColumn('number', 'Y');
                }
                coordsService.getCoords().then(function (coords) {
                    var data = [];
                    var x;
                    var y;
                    for (var c in coords.data) {
                        if (coords.data[c].fields.x) {
                            x = coords.data[c].fields.x;
                        } else {
                            x = null;
                        }

                        if (coords.data[c].fields.y) {
                            y = coords.data[c].fields.y;
                        } else {
                            y = null;
                        }
                        data.push([x, y]);
                    }
                    scope.data.addRows(data);
                    scope.chart.draw(scope.data, scope.options);
                });
            };
        }
    };
});