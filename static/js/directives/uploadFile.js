app.directive('uploadFile', function (fileService) {
    return {
        restrict: 'E',
        templateUrl: '/static/js/template/uploadFile.html',
        link: function (scope, element, attrs) {
            scope.sendFile = function () {
                if (!fileService.isEmpty()) {
                    fileService.uploadFile();
                } else {
                    alert('Загрузите файл');
                }
            }
        }
    }
});