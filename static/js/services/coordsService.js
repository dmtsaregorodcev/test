app.service('coordsService', function ($http) {
    this.getCoords = function () {
        var url = '/coords/';
        return $http.get(url);
    }
});