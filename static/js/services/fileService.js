app.service('fileService', function ($http,$timeout) {
    var _file = null;
    this.setFile = function (file) {
        _file = file;
    };
    this.isEmpty = function () {
        return _file === null;
    };
    this.uploadFile = function () {
        var fd = new FormData();
        var uploadUrl = '/upload/';
        fd.append('file', _file);
        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).success(function (data) {
            $timeout(function(){
                alert(data.message);
            });
        }).error(function () {
            alert('Ошибка');
        });
    }
});